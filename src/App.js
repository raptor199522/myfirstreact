import userPost from './json/posts.json'
import React from 'react'

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.data = JSON.parse(localStorage.getItem('myData'));

        if (this.data === null || this.data.length === 0) {
            localStorage.setItem('myData', JSON.stringify(userPost))
            this.data = userPost
        }

        this.state = {
            items: this.data,
            title: '',
            body: '',
            tags: '',
        };
    }

    remove = i => {
        const idx = this.data.findIndex(el => el.id === i)
        if (idx >= 0) {
            this.data.splice(idx, 1)
            this.setState({items: this.data});
            localStorage.setItem('myData', JSON.stringify(this.data))
        }
    }

    add = (title, body, tags) => {
        this.data.push({title, body, tags})
        this.setState({items: this.data});
        localStorage.setItem('myData', JSON.stringify(this.data))
    }

    addNewPost = (e) => {
        e.preventDefault()
        if (this.state.title.length >= 5 && this.state.body.length >= 25 && this.state.tags.length >= 3) {
            this.add(this.state.title, this.state.body, this.state.tags.split(',').map(el => el.trim()))
            this.setState({title: ''})
            this.setState({body: ''})
            this.setState({tags: ''})
        }

    }

    render() {

        return (
            <div>
                {this.state.items.map((el, i) => {
                    return (
                        <article key={i}>
                            <header>
                                <h3>{el.title}</h3>
                            </header>
                            <section>
                                <p>{el.body}</p>
                            </section>
                            <footer>
                                <div className="tags" if={el.tags}>
                                    {el.tags.map((tag, it) => {
                                        return (
                                            <button className="btn btn-xs" key={it}>{tag}</button>
                                        )
                                    })}
                                </div>
                            </footer>
                            <div className="controls">
                                <button className="btn btn-danger btn-mini"
                                        onClick={() => this.remove(el.id)}>удалить
                                </button>
                            </div>
                        </article>
                    )
                })}
                <form id="post-add" className="col-lg-4">
                    <div className="form-group mb-1">
                        <input
                            value={this.state.title}
                            onChange={e => this.setState({title: e.target.value})}
                            type="text"
                            className="form-control"
                            placeholder="заголовок"
                        />
                    </div>
                    <div className="form-group mb-1">
                        <input
                            value={this.state.body}
                            onChange={e => this.setState({body: e.target.value})}
                            type="text"
                            className="form-control"
                            placeholder="запись"
                        />
                    </div>
                    <div className="form-group mb-1">
                        <input
                            value={this.state.tags}
                            onChange={e => this.setState({tags: e.target.value})}
                            type="text"
                            className="form-control"
                            placeholder="тег, еще тег"
                        />
                    </div>
                    <button onClick={this.addNewPost}
                            className="btn btn-primary">
                        Добавить
                    </button>
                </form>
            </div>
        )
    }
}

function App() {

    return (
        <div className="App">
            <div className="reportWebVitals container">
                <section>
                    <div id="posts" className="well">
                        <Post/>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default App;
